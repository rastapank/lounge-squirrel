# lounge-squirrel [![pipeline status](https://gitlab.com/rastapank/lounge-squirrel/badges/main/pipeline.svg)](https://gitlab.com/rastapank/lounge-squirrel/-/commits/main) [![coverage report](https://gitlab.com/rastapank/lounge-squirrel/badges/main/coverage.svg)](https://gitlab.com/rastapank/lounge-squirrel/-/commits/main) [![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/) [![linting: pylint](https://img.shields.io/badge/linting-pylint-yellowgreen)](https://github.com/PyCQA/pylint) [![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black) [![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)

## 🐿️ Overview

Lounge Squirrel is the 7nth member of Rastapank tech team.

It sits in the Rastapank lounge and helps out with different automation-related tasks.

What a cute little squirrel, that is.

## ✨ Features

- [x] Send connected wifi users to matrix
- [ ] Restart servers
- [ ] Report audio-scheduler related info
- [ ] Turn the lights on/off
- [ ] Do jumping jacks
- [ ] ...

## 🐭 How It Works

Lounge Squirrel is meant to be externally invoked on an interval. See [Fresh Deployment](#fresh-deployment) for details.

Every time it runs:

1. Grabs the mac addresses currently connected to wifi.
2. Maps the mac addresses to hostnames via `/tmp/dhcp.leases`.
3. Maps hostnames to usernames via `deviceUserMap.json`. Hostnames that don't have an associated username are mapped to a 🐭.
4. If the connected usernames have not changed with respect to the last time Lounge Squirrel was invoked, it exits. Otherwise, it sends the diff to a matrix channel in Rastapank space.

At night, it falls asleep and does not execute the above steps.

## 🛠️ Development

### Dependencies

You will need [Python 3.9+](https://www.python.org/downloads/) and the latest Node LTS version, which you can use [n](https://github.com/tj/n) to install:

```bash
curl -L https://raw.githubusercontent.com/tj/n/master/bin/n -o n
bash n lts
```

### Workflow

Get the code:

```bash
git clone https://gitlab.com/rastapank/lounge-squirrel.git
```

Install dev dependencies:

```bash
cd lounge-squirrel
npm i
```

This project uses git hooks via [husky](https://typicode.github.io/husky/#/). Once you make a commit:

- Code formatters (black, prettier) run on staged files
- Code linter (pylint) runs on staged files
- All unit tests run automatically
- [Commitizen](https://github.com/commitizen/cz-cli) will help you write a good commit message. You just follow the prompts and answer the questions that come up.

### Continuous Integration

This project has a Continuous Integration (CI) [pipeline](https://gitlab.com/rastapank/lounge-squirrel/-/blob/main/.gitlab-ci.yml). Every time a PR opens, all unit and integration tests for the PR branch run in a docker container. For the PR to be merged, all tests in CI should pass or you have a good story to tell.

## 🚀 Deployment

Lounge Squirrel is designed to run on the router so it can grab the currently connected mac addresses. The router should have [Python 3.9+](https://www.python.org/downloads/).

Normally you only have to do a [fresh deployment](#fresh-deployment) once, then [the CD](#continuous-deployment) kicks in.

### Fresh Deployment

Login to the router as root. Then do:

```bash
cd /srv/playground
git clone https://gitlab.com/rastapank/lounge-squirrel.git
cd lounge-squirrel
cp .env.template .env # <-- edit this file with production values
cp -r data.template data
```

You may add some users to data/deviceUserMap.json or leave it empty. See `mocks/deviceUserMap.json` for syntax.

You can invoke it once to test it by running `./invokeLoungeSquirrel.sh`

Lounge Squirrel is meant to be externally invoked on an interval. You can use cron to invoke it every minute, like this:

1. `crontab -e`
2. Write `* * * * * /srv/playground/lounge-squirrel/invokeLoungeSquirrel.sh`
3. Press Esc + :wq (save and exit on vim)

Verify the command is written in cron with `crontab -l`. You should see `* * * * * /srv/playground/lounge-squirrel/invokeLoungeSquirrel.sh` in the output.

### Continuous Deployment

This project has a simple Continuous Deployment (CD) [pipeline](https://gitlab.com/rastapank/lounge-squirrel/-/blob/main/invokeLoungeSquirrel.sh). Every time Lounge Squirrel is invoked, the latest code from main branch is being fetched first. This way the latest code is automatically deployed upon a merge of a PR to main branch.
