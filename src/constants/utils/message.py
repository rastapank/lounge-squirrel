"""
Message module constants.
"""
joined_messages = [
    "joined the lounge",
    "entered like a badass",
    "came in to rock",
    "είδε φως και μπήκε",
    "ήρθε να αράξει",
    "εμφανίστηκε μέσα",
    "μπούκαρε",
    "has spawned",
    "joined the party",
    "just landed",
]
left_messages = [
    "left the lounge",
    "exited the station",
    "την έκανε",
    "όπου φύγει, φύγει",
    "έβγαλε φτερά",
    "εξαφανίστηκε",
    "έγινε καπνός",
    ", γύρνα πίσω ή έστω τηλεφώνα",
    "πούλεψε",
    "left the party",
    "has departed",
]
