"""
Main module. The entry point of the program.
"""
from typing import Set
from datetime import datetime, time

from .model.user import User
from .utils.env import is_env_ready, load_env, on_testing
from .utils.message import create_message, send_message
from .utils.network import find_mac_addresses
from .utils.user import (
    connected_devices_changed,
    connected_users_changed,
    find_gone_users,
    find_new_users,
    get_connected_users,
    get_last_connected_users,
    is_sleeping,
    set_sleeping,
    get_usernames_from_users,
    save_connected_users,
)


def main():
    """
    Contains the main logic of the program. Glues function calls together.
    """
    load_env()
    if not is_env_ready():
        return

    if not on_testing():
        now = datetime.now()
        now_time = now.time()
        if now_time < time(8, 00):
            if not is_sleeping():
                message = "It's midnight. Yawwwwwhn, I'm going to sleep 🛏️ 😴 See you tomorrow."
                send_message(message)
                set_sleeping(True)
            return

        if is_sleeping():
            message = (
                "Good morning, Rastapank ! 🦫 just woke up and sitting in the lounge."
            )
            send_message(message)
            set_sleeping(False)

    mac_addresses: Set[str] = find_mac_addresses()
    connected_users: Set[User] = get_connected_users(mac_addresses)
    last_connected_users: Set[User] = get_last_connected_users()

    connected_user_names: Set[str] = get_usernames_from_users(connected_users)
    last_connected_user_names: Set[str] = get_usernames_from_users(last_connected_users)

    # Exit if there is no change in who is connected
    if not connected_users_changed(connected_user_names, last_connected_user_names):
        if connected_devices_changed(connected_users, last_connected_users):
            save_connected_users(connected_users)
        return

    new_users: Set[str] = find_new_users(
        connected_user_names, last_connected_user_names
    )
    gone_users: Set[str] = find_gone_users(
        connected_user_names, last_connected_user_names
    )
    message: str = create_message(connected_users, new_users, gone_users)
    send_message(message)
    save_connected_users(connected_users)


if __name__ == "__main__":
    main()
