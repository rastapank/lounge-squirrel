"""
The user model.
"""
from typing import Set, List, Any


class User:
    """
    Models a user.
    """

    def __init__(self, name: str, devices: Set[str]):
        self.name = name
        self.devices = devices

    def __eq__(self, other):
        if not isinstance(other, User):
            # don't attempt to compare against unrelated types
            return NotImplemented
        return self.name == other.name and self.devices == other.devices

    def __hash__(self):
        # necessary for instances to behave sanely in dicts and sets.
        devices: List[str] = list(self.devices)
        devices.sort()
        devices_string = "-".join(devices)
        return hash((self.name, devices_string))

    def __str__(self):
        return f"{self.name}-{self.devices}"

    def to_json(self) -> dict[str, Any]:
        """
        Returns a JSON representation of the user.
        """
        devices: List[str] = list(self.devices)
        devices.sort()
        user = {"name": self.name, "devices": devices}
        return user
