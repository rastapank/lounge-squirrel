"""
Environment module.
Contains all functions related to environment manipulation.
"""
from os import environ
from os.path import isfile


def load_env():
    """
    Loads variables from .env file to the environment.
    """
    if isfile(".env"):
        with open(".env", "r", encoding="utf-8") as env_file:
            for line in env_file:
                (key, value) = line.strip().split("=")
                environ[key] = value


def is_env_ready() -> bool:
    """
    Checks if all necessary environment variables are set.
    """
    if not environ.get("LOUNGE_SQUIRREL_WEBHOOK_API_KEY"):
        print("LOUNGE_SQUIRREL_WEBHOOK_API_KEY not found in environment.")
        return False
    if not environ.get("LOUNGE_SQUIRREL_WEBHOOK_URL"):
        print("LOUNGE_SQUIRREL_WEBHOOK_URL not found in environment.")
        return False
    return True


def on_testing() -> bool:
    """
    Determines if we are on testing environment.
    """
    return environ.get("LOUNGE_SQUIRREL_TESTING") == "true"


def get_environment_variable(variable_name: str) -> str:
    """
    Returns the value of an environment variable, or the empty string
    if the variable is not found.
    """
    value = environ.get(variable_name)
    if value:
        return value
    return ""


def get_api_key() -> str:
    """
    Retrieves the api key from the environment.
    """
    return get_environment_variable("LOUNGE_SQUIRREL_WEBHOOK_API_KEY")


def get_webhook_url() -> str:
    """
    Retrieves the webhook url from the environment.
    """
    return get_environment_variable("LOUNGE_SQUIRREL_WEBHOOK_URL")
