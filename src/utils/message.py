"""
Message module.
Contains all functions related to message manipulation.
"""
from typing import Set
from urllib.request import Request, urlopen
from json import dumps
from random import choice


from .user import is_user_anonymous
from ..model.user import User
from .env import get_api_key, get_webhook_url
from ..constants.utils.message import joined_messages
from ..constants.utils.message import left_messages


def create_message(
    connected_users: Set[User], new_users: Set[str], gone_users: Set[str]
) -> str:
    """
    Constructs the message to be sent.
    """
    message = ""

    for user in new_users:
        if is_user_anonymous(user):
            message += f"🟢 🐭 {get_random_joined_message()}.\n"
            continue
        message += f"🟢 {user} {get_random_joined_message()}.\n"
    for user in gone_users:
        if is_user_anonymous(user):
            message += f"🔴 🐭 {get_random_left_message()}.\n"
            continue
        message += f"🔴 {user} {get_random_left_message()}.\n"
    if not connected_users:
        message += "\nLounge is empty. All users have left."
        return message
    num_of_people = len(connected_users)
    message += f"\n**{num_of_people}** in lounge: "
    anonymous_users = 0
    for user in connected_users:
        if is_user_anonymous(user.name):
            anonymous_users += 1
            continue
        message += f"{user.name} "
    if anonymous_users == 1:
        rat_message = "🐭 μικρός αρουραίος."
        if anonymous_users != num_of_people:
            rat_message = "και " + rat_message
        message += rat_message
    if anonymous_users > 1:
        rats = ""
        for _ in range(anonymous_users):
            rats += "🐭 "
        rat_message = f"{rats} μικροί αρουραίοι."
        if anonymous_users != num_of_people:
            rat_message = "και " + rat_message
        message += rat_message

    return message


def get_formatted_message(message: str) -> str:
    """
    Formats the message so matrix-webhook does not drop newlines.
    Related bug: https://github.com/nim65s/matrix-webhook/issues/29#issuecomment-1239335040
    """
    return "\n\n".join(message.splitlines())


def send_message(message: str):
    """
    Sends a message to matrix-webhook (https://github.com/nim65s/matrix-webhook).
    """
    data = {
        "key": get_api_key(),
        "body": get_formatted_message(message),
    }
    postdata = dumps(data).encode()
    with urlopen(Request(get_webhook_url(), data=postdata)):
        pass


def get_random_joined_message() -> str:
    """
    Returns a "user joined" message at random.
    """
    return choice(joined_messages)


def get_random_left_message() -> str:
    """
    Returns a "user left" message at random.
    """
    return choice(left_messages)
