"""
Network module.
Contains all functions related to network manipulation.
"""
from typing import Set
from json import load

from .env import on_testing
from .other import exec_bash_command, get_device_user_map_file_path, sanitize_input


def get_mac_addresses_command() -> str:
    """
    Returns the bash command that finds the mac addresses.
    """
    command = "iw dev wlan0 station dump"
    if on_testing():
        command = "cat mocks/macAddresses.txt"
    return f'{command} | grep Station | cut -d " " -f 2'


def find_mac_addresses() -> Set[str]:
    """
    Returns the mac addresses currently connected to wifi.
    """
    mac_addresses = exec_bash_command(get_mac_addresses_command()).splitlines()
    mac_addresses = map(sanitize_input, mac_addresses)
    return set(mac_addresses)


def get_device_name_from_mac_address_command(mac_address: str) -> str:
    """
    Returns the bash command that finds the name of the device
    with the given mac address.
    """
    file_path = "/tmp/dhcp.leases"
    if on_testing():
        file_path = "mocks/dhcpLeases.txt"
    mac_address = sanitize_input(mac_address)
    return f'cat {file_path} | grep {mac_address} | cut -d " " -f 4'


def get_device_name_from_mac_address(mac_address: str) -> str:
    """
    Returns the name of the device with the given mac address.
    """
    return sanitize_input(
        exec_bash_command(get_device_name_from_mac_address_command(mac_address))
    )


def get_user_name_from_device_name(device_name: str) -> str:
    """
    Returns the user of the device.
    """
    device_name = sanitize_input(device_name)
    with open(get_device_user_map_file_path(), "r", encoding="utf-8") as file:
        device_user_map = load(file)
    return device_user_map.get(device_name, "")
