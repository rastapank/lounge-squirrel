"""
Other module.
Contains all functions that do not fit into any other module (yet).
"""
from json import dump
from subprocess import run


from .env import on_testing


def exec_bash_command(command: str) -> str:
    """
    Executes a bash command and returns the output.
    """
    return run(
        command, shell=True, capture_output=True, check=False, encoding="UTF-8"
    ).stdout.strip()


def sanitize_input(input_string: str) -> str:
    """
    Sanitizes the input to prevent bash command injection.
    """
    input_string = "_".join(input_string.split())
    if not input_string:
        input_string = "(empty)"
    return input_string


def restore_last_connected_users_file():
    """
    Empties the last connected users file.
    """
    with open(get_last_connected_users_file_path(), "w", encoding="utf-8"):
        pass


def restore_sleep_file():
    """
    Edits the sleep file so sleeping is False.
    """
    sleep = {"sleeping": False}
    with open(get_sleep_file_path(), "w", encoding="utf-8") as file:
        dump(sleep, file, indent=2)


def get_device_user_map_file_path() -> str:
    """
    Retrieves the path of device-user map file.
    """
    if on_testing():
        return "mocks/deviceUserMap.json"
    return "data/deviceUserMap.json"


def get_last_connected_users_file_path() -> str:
    """
    Retrieves the path of last connected users file.
    """
    if on_testing():
        return "mocks/lastConnectedUsers.json"
    return "data/lastConnectedUsers.json"


def get_sleep_file_path() -> str:
    """
    Retrieves the path of sleep file.
    """
    if on_testing():
        return "mocks/sleep.json"
    return "data/sleep.json"
