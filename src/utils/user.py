"""
User module.
Contains all functions related to user manipulation.
"""
from typing import Set, List, Optional, Any
from copy import deepcopy
from json import dump, load
from os import stat

from .other import get_last_connected_users_file_path, get_sleep_file_path

from ..model.user import User
from .network import get_user_name_from_device_name
from .network import get_device_name_from_mac_address
from ..constants.utils.user import ANONYMOUS_USER_PREFIX


def is_user_anonymous(username: str) -> bool:
    """
    Returns True if the user is anonymous.
    """
    return username.startswith(f"{ANONYMOUS_USER_PREFIX}-")


def create_anonymous_connected_username(device_name: str) -> str:
    """
    Creates an anonymous username from a device name.
    """
    return f"{ANONYMOUS_USER_PREFIX}-{device_name}"


def create_connected_user(device_name: str, username: str) -> User:
    """
    Creates and returns a connected user.
    """
    if username:
        return User(username, set([device_name]))
    return User(create_anonymous_connected_username(device_name), set([device_name]))


def add_connected_user(connected_users: Set[User], connected_user: User):
    """
    Adds a connectedUser to connecterUsers set, or updates the user
    if it already exists.
    """
    user_list = list(
        filter(lambda user: user.name == connected_user.name, connected_users)
    )
    if not user_list:
        connected_users.add(connected_user)
        return
    user = user_list[0]
    new_user = deepcopy(user)
    new_user.devices.update(connected_user.devices)
    connected_users.remove(user)
    connected_users.add(new_user)


def get_connected_users(mac_addresses: Set[str]) -> Set[User]:
    """
    Returns the connected users from their mac addresses.
    If a username is not found, an anonymous user is returned instead.
    """
    connected_users: Set[User] = set()

    for mac_address in mac_addresses:
        device_name: str = get_device_name_from_mac_address(mac_address)
        username: str = get_user_name_from_device_name(device_name)
        connected_user: User = create_connected_user(device_name, username)
        add_connected_user(connected_users, connected_user)

    return connected_users


def get_last_connected_users_from_file() -> Optional[List[dict[str, Any]]]:
    """
    Parses the last connected users file and returns the user objects.
    """
    if stat(get_last_connected_users_file_path()).st_size == 0:
        return None
    with open(get_last_connected_users_file_path(), "r", encoding="utf-8") as file:
        return load(file)


def get_last_connected_users() -> Set[User]:
    """
    Returns the connected users the last time the program was run.
    """
    last_connected_users_list = get_last_connected_users_from_file()
    if not last_connected_users_list:
        return set()
    last_connected_users: Set[User] = set()

    for user in last_connected_users_list:
        for device in user["devices"]:
            last_connected_user: User = create_connected_user(device, user["name"])
            add_connected_user(last_connected_users, last_connected_user)

    return last_connected_users


def get_usernames_from_users(users: Set[User]) -> Set[str]:
    """
    Returns the user names from user objects.
    """
    return set(map(lambda user: user.name, users))


def connected_devices_changed(
    connected_users: Set[User], last_connected_users: Set[User]
):
    """
    Determines if the connected devices have changed with respect
    to the last time the program was run.
    """
    return connected_users != last_connected_users


def connected_users_changed(
    connected_users: Set[str], last_connected_users: Set[str]
) -> bool:
    """
    Determines if the connected users have changed with respect
    to the last time the program was run.
    """
    return connected_users != last_connected_users


def find_new_users(
    connected_users: Set[str], last_connected_users: Set[str]
) -> Set[str]:
    """
    Finds the users that have joined the wifi after the last time the program was run.
    """
    return connected_users - last_connected_users


def find_gone_users(
    connected_users: Set[str], last_connected_users: Set[str]
) -> Set[str]:
    """
    Finds the users that have left the wifi after the last time the program was run.
    """
    return last_connected_users - connected_users


def save_connected_users(connected_users: Set[User]):
    """
    Saves the connected users to a file.
    This is done so they can be compared to currently connected users,
    to determine if there is a change.
    """
    users: List[dict[str, Any]] = list(
        map(lambda user: user.to_json(), list(connected_users))
    )
    with open(get_last_connected_users_file_path(), "w", encoding="utf-8") as file:
        dump(users, file, indent=2)


def is_sleeping() -> bool:
    """
    Gets the sleeping flag.
    This is done so lounge squirrel can know if it is sleeping.
    """
    with open(get_sleep_file_path(), "r", encoding="utf-8") as file:
        sleep = load(file)
    return sleep["sleeping"] is True


def set_sleeping(sleeping: bool):
    """
    Saves a sleeping flag.
    This is done so lounge squirrel can sleep at night.
    """
    sleep = {"sleeping": False}
    if sleeping:
        sleep["sleeping"] = True
    with open(get_sleep_file_path(), "w", encoding="utf-8") as file:
        dump(sleep, file, indent=2)
