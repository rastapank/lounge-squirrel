"""
Tests the main module.
"""
from os import environ, stat
from json import load, dump

import pytest

from src.main import main
from src.utils.other import (
    get_last_connected_users_file_path,
    restore_last_connected_users_file,
)


@pytest.fixture(autouse=True)
def run_around_tests():
    """
    Sets testing flag to true before each test
    and resets last connected users file after each test.
    """
    environ["LOUNGE_SQUIRREL_TESTING"] = "true"
    yield
    restore_last_connected_users_file()


def test_main_users_changed():
    """
    Tests the output of the program when the users have changed.
    """
    assert stat(get_last_connected_users_file_path()).st_size == 0
    main()
    assert stat(get_last_connected_users_file_path()).st_size != 0
    with open(get_last_connected_users_file_path(), "r", encoding="utf-8") as file:
        user_list = load(file)
    assert len(user_list) == 4
    user_list = list(map(str, user_list))
    user_list.sort()
    user_set = set(user_list)
    expected_user_set = set(
        [
            "{'name': 'arouraios-device4', 'devices': ['device4']}",
            "{'name': 'arouraios-device5', 'devices': ['device5']}",
            "{'name': 'username1', 'devices': ['device1', 'device2']}",
            "{'name': 'username2', 'devices': ['device3']}",
        ]
    )
    assert user_set == expected_user_set


def test_main_users_not_changed():
    """
    Tests the output of the program when the users have not changed.
    """
    assert stat(get_last_connected_users_file_path()).st_size == 0
    users_to_write = [
        {"name": "username2", "devices": ["device3"]},
        {"name": "arouraios-device4", "devices": ["device4"]},
        {"name": "username1", "devices": ["device1", "device2"]},
        {"name": "arouraios-device5", "devices": ["device5"]},
    ]
    with open(get_last_connected_users_file_path(), "w", encoding="utf-8") as file:
        dump(users_to_write, file, indent=2)
    assert stat(get_last_connected_users_file_path()).st_size != 0
    main()
    assert stat(get_last_connected_users_file_path()).st_size != 0
    with open(get_last_connected_users_file_path(), "r", encoding="utf-8") as file:
        users = load(file)
    assert users == users_to_write
