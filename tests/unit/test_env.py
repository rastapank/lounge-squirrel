"""
Tests the env module.
"""
from os import environ

import pytest

from src.utils.env import (
    get_api_key,
    get_environment_variable,
    get_webhook_url,
    is_env_ready,
    on_testing,
)


@pytest.fixture(autouse=True)
def run_before_each_test():
    """
    Removes all vars from the environment.
    """
    environ.clear()


def test_is_env_ready_case_all_vars_exist():
    """
    Tests if env is ready when all vars exist.
    """
    environ["LOUNGE_SQUIRREL_WEBHOOK_API_KEY"] = "testApiKey"
    environ["LOUNGE_SQUIRREL_WEBHOOK_URL"] = "testWebhookUrl"
    assert is_env_ready()


def test_is_env_ready_missing_api_key():
    """
    Tests if env is not ready when the webhook api key is missing.
    """
    environ["LOUNGE_SQUIRREL_WEBHOOK_URL"] = "testWebhookUrl"
    assert not is_env_ready()


def test_is_env_ready_missing_webhook_url():
    """
    Tests if env is not ready when the webhook url is missing.
    """
    environ["LOUNGE_SQUIRREL_WEBHOOK_API_KEY"] = "testApiKey"
    assert not is_env_ready()


def test_is_env_ready_case_missing_all():
    """
    Tests if env is not ready when every var is missing.
    """
    assert not is_env_ready()


def test_on_testing_case_true():
    """
    Tests if it can detect that is on testing env.
    """
    environ["LOUNGE_SQUIRREL_TESTING"] = "true"
    assert on_testing()


def test_on_testing_case_false():
    """
    Tests if it can detect that is not on testing env when testing flag is False.
    """
    environ["LOUNGE_SQUIRREL_TESTING"] = "false"
    assert not on_testing()


def test_on_testing_case_missing():
    """
    Tests if it can detect that is not on testing env when testing flag is not set.
    """
    assert not on_testing()


def test_get_environment_variable():
    """
    Tests if it can read env variables.
    """
    environ["TEST_ENV_VARIABLE"] = "TEST_ENV_VARIABLE_VALUE"
    assert get_environment_variable("TEST_ENV_VARIABLE") == "TEST_ENV_VARIABLE_VALUE"
    assert get_environment_variable("TEST_ENV_VARIABLE_2") == ""


def test_get_api_key():
    """
    Tests if it can read the webhook api key from the environment.
    """
    assert get_api_key() == ""
    environ["LOUNGE_SQUIRREL_WEBHOOK_API_KEY"] = "TEST_API_KEY"
    assert get_api_key() == "TEST_API_KEY"


def test_get_webhook_url():
    """
    Tests if it can read the webhook url from the environment.
    """
    assert get_webhook_url() == ""
    environ["LOUNGE_SQUIRREL_WEBHOOK_URL"] = "TEST_WEBHOOK_URL"
    assert get_webhook_url() == "TEST_WEBHOOK_URL"
