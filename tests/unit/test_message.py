"""
Tests the message module.
"""
from os import environ

import pytest

from src.utils.message import get_formatted_message


@pytest.fixture(autouse=True)
def run_before_each_test():
    """
    Sets testing flag to true.
    """
    environ["LOUNGE_SQUIRREL_TESTING"] = "true"


def test_get_formatted_message():
    """
    Tests the formatting of a message.
    """
    message = """This
is
a
message"""
    assert (
        get_formatted_message(message)
        == """This

is

a

message"""
    )
