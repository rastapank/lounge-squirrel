"""
Tests the network module.
"""
from os import environ

import pytest

from src.utils.network import (
    get_mac_addresses_command,
    get_device_name_from_mac_address_command,
    find_mac_addresses,
    get_device_name_from_mac_address,
    get_user_name_from_device_name,
)


@pytest.fixture(autouse=True)
def run_before_each_test():
    """
    Sets testing flag to true.
    """
    environ["LOUNGE_SQUIRREL_TESTING"] = "true"


def test_get_mac_addresses_command_case_on_testing():
    """
    Tests getting the mac addresses command when on testing.
    """
    environ["LOUNGE_SQUIRREL_TESTING"] = "true"
    assert (
        get_mac_addresses_command()
        == 'cat mocks/macAddresses.txt | grep Station | cut -d " " -f 2'
    )


def test_get_mac_addresses_command_case_on_prod():
    """
    Tests getting the mac addresses command when on prod.
    """
    environ["LOUNGE_SQUIRREL_TESTING"] = "false"
    assert (
        get_mac_addresses_command()
        == 'iw dev wlan0 station dump | grep Station | cut -d " " -f 2'
    )


def test_get_device_name_from_mac_address_command_case_on_testing():
    """
    Tests getting the device name from mac addresses command when on testing.
    """
    environ["LOUNGE_SQUIRREL_TESTING"] = "true"
    assert (
        get_device_name_from_mac_address_command("testMacAddress")
        == 'cat mocks/dhcpLeases.txt | grep testMacAddress | cut -d " " -f 4'
    )


def test_get_device_name_from_mac_address_command_case_on_prod():
    """
    Tests getting the device name from mac addresses command when on prod.
    """
    environ["LOUNGE_SQUIRREL_TESTING"] = "false"
    assert (
        get_device_name_from_mac_address_command("testMacAddress")
        == 'cat /tmp/dhcp.leases | grep testMacAddress | cut -d " " -f 4'
    )


def test_find_mac_addresses():
    """
    Tests finding the mac addresses of connected devices.
    """
    assert find_mac_addresses() == set(
        [
            "ff:e8:52:a3:a9:7a",
            "ff:e8:52:a3:a9:7b",
            "ff:e8:52:a3:a9:7c",
            "ff:e8:52:a3:a9:7d",
            "ff:e8:52:a3:a9:7e",
        ]
    )


def test_get_device_name_from_mac_address():
    """
    Tests getting the device name from a mac address.
    """
    assert get_device_name_from_mac_address("ff:e8:52:a3:a9:7a") == "device1"


def test_get_user_name_from_device_name():
    """
    Tests getting the user name from a device name.
    """
    assert get_user_name_from_device_name("device1") == "username1"
