"""
Tests the other module.
"""
from os import environ

import pytest

from src.utils.other import exec_bash_command, sanitize_input


@pytest.fixture(autouse=True)
def run_before_each_test():
    """
    Sets testing flag to true.
    """
    environ["LOUNGE_SQUIRREL_TESTING"] = "true"


def test_exec():
    """
    Tests the execution of a bash command.
    """
    command = "echo test"
    assert exec_bash_command(command) == "test"


def test_sanitize_input():
    """
    Tests the sanitization of commands with spaces.
    """
    command = "ls -lah_"
    assert sanitize_input(command) == "ls_-lah_"
    command = "ls; pwd"
    assert sanitize_input(command) == "ls;_pwd"
