"""
Tests the user module.
"""
from os import environ, stat
from typing import Set
from json import load, dump


import pytest


from src.model.user import User
from src.utils.user import (
    add_connected_user,
    connected_devices_changed,
    connected_users_changed,
    create_anonymous_connected_username,
    find_gone_users,
    find_new_users,
    create_connected_user,
    get_connected_users,
    get_last_connected_users,
    get_last_connected_users_from_file,
    get_usernames_from_users,
    is_sleeping,
    is_user_anonymous,
    save_connected_users,
    set_sleeping,
)
from src.utils.other import (
    get_last_connected_users_file_path,
    get_sleep_file_path,
    restore_last_connected_users_file,
    restore_sleep_file,
)


@pytest.fixture(autouse=True)
def run_before_each_test():
    """
    Sets testing flag to true.
    """
    environ["LOUNGE_SQUIRREL_TESTING"] = "true"


def test_is_user_anonymous_case_true():
    """
    Tests if an anonymous user is anonymous.
    """
    assert is_user_anonymous("arouraios-device1")


def test_is_user_anonymous_case_false():
    """
    Tests if a non-anonymous user is not anonymous.
    """
    assert not is_user_anonymous("username1-device1")


def test_create_anonymous_connected_user_name():
    """
    Tests the creation of an anonymous username.
    """
    assert create_anonymous_connected_username("device1") == "arouraios-device1"


def test_create_connected_user():
    """
    Tests the creation of a connected user.
    """
    assert create_connected_user("device1", "username1") == User(
        "username1", set(["device1"])
    )


def test_create_connected_user_anonymous():
    """
    Tests the creation of an anonymous connected user.
    """
    assert create_connected_user("device1", "") == User(
        "arouraios-device1", set(["device1"])
    )


def test_add_connected_user_case_to_empty_set():
    """
    Tests the addition of a connected user to an empty set.
    """
    connected_users: Set[User] = set()
    connected_user = User("username1", set(["device1"]))
    add_connected_user(connected_users, connected_user)
    assert connected_user in connected_users


def test_add_connected_user_case_add_device():
    """
    Tests the addition of a device to an already existing user.
    """
    connected_users = set([User("username1", set(["device1"]))])
    connected_user = User("username1", set(["device2"]))
    add_connected_user(connected_users, connected_user)
    user = list(connected_users)[0]
    assert user.name == "username1"
    assert user.devices == set(["device1", "device2"])


def test_get_connected_users():
    """
    Tests getting the connected users from mac addresses.
    """
    assert get_connected_users(
        set(["ff:e8:52:a3:a9:7a", "ff:e8:52:a3:a9:7b", "ff:e8:52:a3:a9:7c"])
    ) == set(
        [
            User("username1", set(["device1", "device2"])),
            User("username2", set(["device3"])),
        ]
    )


def test_get_last_connected_users():
    """
    Tests getting the last connected users.
    """
    assert get_last_connected_users() == set([])


def test_get_last_connected_users_case_empty_file():
    """
    Tests getting the last connected users from file.
    """
    assert get_last_connected_users_from_file() is None


def test_get_user_names_from_users():
    """
    Tests getting user names from users.
    """
    assert get_usernames_from_users(
        set([User("username1", set(["device1"])), User("username2", set(["device2"]))])
    ) == set(["username1", "username2"])


def test_connected_devices_changed_case_device_changed():
    """
    Tests if a device change of a user is detected.
    """
    connected_users = set([User("username1", set(["device1"]))])
    last_connected_users = set([User("username1", set(["device2"]))])
    assert connected_devices_changed(connected_users, last_connected_users)


def test_connected_devices_changed_case_device_added():
    """
    Tests if a device addition of a user is detected.
    """
    connected_users = set([User("username1", set(["device1"]))])
    last_connected_users = set([User("username1", set(["device1", "device2"]))])
    assert connected_devices_changed(connected_users, last_connected_users)


def test_connected_devices_changed_case_false():
    """
    Tests if no change in devices is detected.
    """
    connected_users = set([User("username1", set(["device1"]))])
    last_connected_users = set([User("username1", set(["device1"]))])
    assert not connected_devices_changed(connected_users, last_connected_users)


def test_connected_users_changed_case_true():
    """
    Tests if a change in a user is detected.
    """
    assert connected_users_changed(
        set(["username1", "username2"]), set(["username1", "username3"])
    )


def test_connected_users_changed_case_false():
    """
    Tests if no change in users is detected.
    """
    assert not connected_users_changed(
        set(["username1", "username2"]), set(["username1", "username2"])
    )


def test_find_new_users():
    """
    Tests if a new user is detected.
    """
    assert find_new_users(
        set(["username1", "username2"]), set(["username1", "username3"])
    ) == set(["username2"])


def test_find_gone_users():
    """
    Tests if a user that has left is detected.
    """
    assert find_gone_users(
        set(["username1", "username2"]), set(["username1", "username3"])
    ) == set(["username3"])


def test_save_connected_users():
    """
    Tests if the connected users can be saved.
    """
    assert stat(get_last_connected_users_file_path()).st_size == 0
    user_set = set(
        [
            User("username1", set(["device1", "device2"])),
            User("username2", set(["device3"])),
        ]
    )
    save_connected_users(user_set)
    assert stat(get_last_connected_users_file_path()).st_size != 0
    with open(get_last_connected_users_file_path(), "r", encoding="utf-8") as file:
        last_connected_users = load(file)
    assert len(last_connected_users) == 2
    last_connected_users = list(map(str, last_connected_users))
    last_connected_users.sort()
    assert last_connected_users == [
        "{'name': 'username1', 'devices': ['device1', 'device2']}",
        "{'name': 'username2', 'devices': ['device3']}",
    ]
    restore_last_connected_users_file()


def test_is_sleeping_case_false():
    """
    Tests if the program is awake.
    """
    assert not is_sleeping()


def test_is_sleeping_case_true():
    """
    Tests if the program is sleeping.
    """
    sleep = {"sleeping": True}
    with open(get_sleep_file_path(), "w", encoding="utf-8") as file:
        dump(sleep, file, indent=2)
    assert is_sleeping()
    restore_sleep_file()


def test_set_sleeping_case_true():
    """
    Tests if sleeping state can be set to True.
    """
    sleep = {}
    with open(get_sleep_file_path(), "r", encoding="utf-8") as file:
        sleep = load(file)
    assert not sleep["sleeping"]
    set_sleeping(True)
    with open(get_sleep_file_path(), "r", encoding="utf-8") as file:
        sleep = load(file)
    assert sleep["sleeping"]
    restore_sleep_file()


def test_set_sleeping_case_false():
    """
    Tests if sleeping state can be set to False.
    """
    sleep = {}
    with open(get_sleep_file_path(), "r", encoding="utf-8") as file:
        sleep = load(file)
    assert not sleep["sleeping"]
    set_sleeping(False)
    with open(get_sleep_file_path(), "r", encoding="utf-8") as file:
        sleep = load(file)
    assert not sleep["sleeping"]
    restore_sleep_file()
